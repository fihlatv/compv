  - Hand-written assembler
  - SIMD (SSE, AVX, NEON)
  - GPGPU
  - Smart multithreading (minimal context switch, no false-sharing, no boundaries crossing..)
  - Smart memory access (data alignment, cache pre-load, cache blocking, non-temporal load/store for minimal cache pollution, smart reference counting...)
  - Fixed-point math
  - ... and many more 

### Online demo apps using our code ###
 - <a target="_blank" href="https://doubango.org/webapps/ocr/">Scene text recognition (TextInWild)</a>
 - <a target="_blank" href="https://doubango.org/webapps/cbir/">Content-Based Image Retrieval (CBIR)</a>
 - <a target="_blank" href="https://doubango.org/webapps/alpr/">Automatic Number Plate Recognition (ANPR/ALPR)</a>
 - <a target="_blank" href="https://doubango.org/webapps/mrz/">Machine-readable zone/passport (MRZ/MRP)</a>
 
 ### Technical questions ###
 Please check our [discussion group](https://groups.google.com/forum/#!forum/doubango-ai)
